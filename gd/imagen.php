<?php
    $imagen = imagecreatefrompng("php.png");

    header("Content-Type: image/png");

    imagealphablending($imagen, true);
    imagesavealpha($imagen, true);

    imagepng($imagen);

    imagedestroy(
        $imagen
    );
?>