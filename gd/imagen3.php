<?php

$img_org = imagecreatefromgif("dwes.gif");

// Obtenemos la mitad del tamaño de la imagen origen.
// Usaremos imagesx e imagesy para obtener las dimensiones.
$ancho_dst = intval(imagesx($img_org) / 2);
$alto_dst = intval(imagesy($img_org) / 2);

// Creamos un lienzo para la imagen destino con las
// dimensiones calculadas.
$img_dst = imagecreatetruecolor($ancho_dst, $alto_dst);

// Escalamos la imagen gif origen sobre la imagen nueva.
// Fíjate que vamos a ocupar todo el lienzo de destino y del
// origen no recortamos nada.
imagecopyresized(
    $img_dst, $img_org,
    0, 0, 0, 0, $ancho_dst, $alto_dst,
    imagesx($img_org), imagesy($img_org));
// Damos salida a la imagen final
// cambiando el formato a png.
header("Content-type: image/gif");
imagegif($img_dst);
// Destruimos ambas imágenes
imagedestroy($img_org);
imagedestroy($img_dst);