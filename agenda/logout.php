<?php
    session_start();

    if (isset($_SESSION['usuario']))
    {
        $_SESSION['usuario'] = null;
        unset($_SESSION['usuario']);

        session_destroy();
    }

    header('location: login.php');