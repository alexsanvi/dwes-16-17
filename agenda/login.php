<?php
session_start();

require("partials/cabecera.php");
?>
<main>
    <form id="login"
          action="<?php echo $_SERVER['PHP_SELF']; ?>"
          method="post">
        <label>Usuario</label>
        <input type="text" name="usuario">
        <label>Password</label>
        <input type="password" name="password">
        <input type="submit" value="Enviar" name="enviar">
    </form>
    <?php
        if (isset($_POST['enviar']))
        {
            if (!empty($_POST['usuario']) && !empty($_POST['password']))
            {
                require_once('bd/usuariobd.inc.php');
                require_once('entity/usuario.inc.php');

                $usuarioBD = new UsuarioBD();

                $usuario = new Usuario($_POST['usuario'], $_POST['password']);

                if ($usuarioBD->checkLogin($usuario) === true)
                {
                    $_SESSION['usuario'] = $_POST['usuario'];
                    header( 'Location: index.php');
                }
                else
                {
                    echo "No existe el usuario o la contraseña no coincide";
                }
            }
            else
            {
                echo "Debes introducir usuario y password";
            }
        }
    ?>
</main>
<?php
    require("partials/pie.php");
?>

