<?php

class Contacto
{
    private $id;
    private $nombre;
    private $telefono;
    private $ciudad;
    private $contactosCiudad;
    private $imagenContacto;

    public function __construct($nombre="", $telefono="", $ciudad="", $contactosCiudad = 0)
    {
        $this->nombre = $nombre;
        $this->telefono = $telefono;
        $this->ciudad = $ciudad;
        $this->contactosCiudad = $contactosCiudad;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    public function getContactosCiudad()
    {
        return $this->contactosCiudad;
    }

    public function setContactosCiudad($contactosCiudad)
    {
        $this->contactosCiudad = $contactosCiudad;
    }

    public function getImagenContacto()
    {
        return $this->imagenContacto;
    }

    public function setImagenContacto($imagenContacto)
    {
        $this->imagenContacto = $imagenContacto;
    }

}