<?php

class Usuario
{
    private $id;
    private $nick;
    private $password;
    private $role;
    private $avatar;
    private $mimeAvatar;

    public function __construct($nick="", $password="", $role="")
    {
        $this->id = -1;
        $this->nick = $nick;
        $this->password = $password;
        $this->role = $role;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNick()
    {
        return $this->nick;
    }

    public function setNick($nick)
    {
        $this->nick = $nick;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getAvatarBase64()
    {
        return $this->avatar;
    }

    public function getAvatar()
    {
        return base64_decode($this->avatar);
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function getMimeAvatar()
    {
        return $this->mimeAvatar;
    }

    public function setMimeAvatar($mimeAvatar)
    {
        $this->mimeAvatar = $mimeAvatar;
    }


}