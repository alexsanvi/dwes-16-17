<?php
    require("partials/checkUserLogued.inc.php");
    require("partials/cabecera.php");
?>
    <main>
    <?php
        require_once('utils/utilsContactos.inc.php');
        require_once('bd/contactobd.inc.php');
        require_once("entity/contacto.inc.php");
        require_once("utils/utilsformulario.inc.php");

        $contactoBD = new ContactoBD();

        if (seHaPulsado('enviar') === true)
        {
            addContacto($contactoBD);
        }
        else
        {
            if (esOperacion('eliminar') === true)
            {
                removeContacto($contactoBD);
            }
            else
            {
                if (seHaPulsado('enviarEdicion') === true)
                {
                    editContacto($contactoBD);
                }
            }
        }

        require('partials/formulario.php');

        if (seHaPulsado('enviarBuscar') === true && !empty($_POST['buscar']))
        {
            $contactos = $contactoBD->buscaContactos($_POST['buscar']);
        }
        else
        {
            $contactos = $contactoBD->getContactos();
        }

        require('partials/ciudades.php');

        if (esOperacion('editar') === true)
        {
            if (checkRoleAdmin() === true)
            {
                echo "<form id='editar' method='post' action='".$_SERVER['PHP_SELF']."?idAnterior=".$_GET['id']."''>";
            }
            else
            {
                $_GET['operacion'] = null;
            }
        }

        require('partials/tablacontactos.php');

        if (esOperacion('editar') === true)
        {
            echo "</form>";
        }
        ?>
    </main>

<?php
    require("partials/pie.php");
?>

