<?php

class BDException extends Exception
{
    public function __construct($consulta)
    {
        $error = $consulta->errorInfo();
        $this->message = $error[0].": ".$error[2];
    }
}