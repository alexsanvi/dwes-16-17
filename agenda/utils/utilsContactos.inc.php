<?php

require_once(__DIR__."/../bd/contactobd.inc.php");
require_once(__DIR__."/../bd/usuariobd.inc.php");
require_once(__DIR__."/../entity/usuario.inc.php");
require_once(__DIR__."/../utils/file.inc.php");

function checkRoleAdmin()
{
    $usuario = new Usuario($_SESSION['usuario']);

    $usuarioBD = new UsuarioBD();
    if ($usuarioBD->findByNick($usuario) === true)
    {
        if ($usuario->getRole() === 'admin')
            return true;
    }

    echo "Lo sentimos, sólo los admin pueden realizar esta operación";

    return false;
}

function addContacto(ContactoBD $bd)
{
    if (checkRoleAdmin() === true)
    {
        if (!empty($_POST['nombre']) && !empty($_POST['telefono']))
        {
            $file = new file('imgContacto');

            $arrTypes = array('image/jpeg', 'image/png', 'image/gif');

            if ($file->isSetFile() === true
                && $file->uploadWithoutErrors()
                && $file->checkTypes($arrTypes) === true
                && $file->saveUploadedFile() === true)
            {
                $nombreImagen = $file->getFileName();

                if (!empty($_POST['ciudad']))
                {
                    $contacto = new Contacto(
                        $_POST['nombre'], $_POST['telefono'], $_POST['ciudad']);

                }
                else
                {
                    $contacto = new Contacto(
                        $_POST['nombre'], $_POST['telefono']);
                }

                $contacto->setImagenContacto($nombreImagen);

                $result = $bd->addContacto($contacto);
                if ($result === true)
                    echo "contacto añadido";
                else
                    echo "error al añadir contacto. ".$bd->getLastError();
            }
            else
            {
                echo "<p>".$file->getLastError()."</p>";
            }
        }
        else
        {
            echo "Debes rellenar el nombre y el teléfono";
        }
    }
}

function removeContacto(ContactoBD $bd)
{
    if (checkRoleAdmin() === true)
    {
        if(isset($_GET['id']) && $_GET['id']  !== '')
        {
            $contacto = $bd->getContactoById($_GET['id']);

            $result = $bd->removeContacto($contacto);
            if ($result === true)
            {
                if (EliminaFicheroImagenContacto($contacto) === true)
                    echo 'Contacto eliminado<br>';
                else
                    echo 'Se ha eliminado el contacto, pero no se ha podido eliminar su imagen';
            }
            else
            {
                echo "error al eliminar contacto. ".$bd->getLastError();
            }
        }
        else
        {
            echo 'No se ha recibido el nombre del contacto a eliminar<br>';
        }
    }
}

function editContacto(ContactoBD $bd)
{
    if (checkRoleAdmin() === true)
    {
        if (!empty($_POST['nombre']))
        {
            $contacto = new Contacto(
                $_POST['nombre'], $_POST['telefono'], $_POST['ciudad']);
            $contacto->setId($_GET['idAnterior']);

            $result = $bd->updateContacto($contacto);
            if ($result === true)
            {
                echo 'Contacto actualizado<br>';
            }
            else
            {
                echo "error al actualizar contacto. " . $bd->getLastError();
            }
        }
    }
}

function getRutaImagenContacto(Contacto $contacto)
{
    return FILE::$RUTA_IMGS_SUBIDAS.$contacto->getImagenContacto();
}

function EliminaFicheroImagenContacto(Contacto $contacto)
{
    $ruta = FILE::$RUTA_IMGS_SUBIDAS.$contacto->getImagenContacto();

    return unlink($ruta);
}
