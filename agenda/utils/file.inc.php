<?php


class File
{
    private $file;
    private $error;
    private $fileName;
    private $mime;
    private $encodedFile;

    public static $RUTA_IMGS_SUBIDAS = './imgs/subidas/';

    public function getFileName()
    {
        return $this->fileName;
    }

    public function __construct($fileName)
    {
        $this->file = $_FILES[$fileName];
        $this->error = "";
        $this->fileName = "";
    }

    public function getLastError()
    {
        return $this->error;
    }

    public function isSetFile()
    {
        if (!isset($this->file))
        {
            $this->error = "Error: Debes seleccionar un fichero";
            return false;
        }

        return true;
    }

    function uploadWithoutErrors()
    {
        if ($this->file['error'] != UPLOAD_ERR_OK)
        {
            switch ($this->file['error'])
            {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    $this->error = "Error: El fichero es demasiado grande";
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $this->error = "Error: El fichero no se ha podido subir entero";
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $this->error = "Error: No se ha podido subir el fichero";
                    break;
                default:
                    $this->error = "Error: Error indeterminado";
                    break;
            }

            return false;
        }

        return true;
    }

    public function checkTypes($arrTypes)
    {
        if (in_array($this->file['type'], $arrTypes) === false)
        {
            $this->error = "Error: Tipo de fichero no soportado";
            return false;
        }

        return true;
    }

    public function saveUploadedFile()
    {
        if (is_uploaded_file($this->file['tmp_name']) === true)
        {
            $this->fileName = $this->file['name'];
            $ruta = self::$RUTA_IMGS_SUBIDAS.$this->fileName;

            if (is_file($ruta) === true)
            {
                $idUnico = time();
                $this->fileName = $idUnico.'_'.$this->file['name'];
                $ruta = self::$RUTA_IMGS_SUBIDAS.$this->fileName;
            }

            if (move_uploaded_file($this->file['tmp_name'], $ruta) === false)
            {
                $this->error = 'Error: No se puede mover el fichero a su destino';
            }
            else
                return true;
        }

        return false;
    }

    public function readEncodedImage()
    {
        $imagen_tmp = $this->file['tmp_name'];

        $fp = fopen($imagen_tmp, "rb");
        $imagen = fread($fp, filesize($imagen_tmp));
        $this->encodedFile = base64_encode($imagen);

        fclose($fp);

        $isize = getimagesize($imagen_tmp);
        if ($isize)
            $this->mime = $isize['mime'];
        else
            $this->mime = 'image';
    }

    public function getMime()
    {
        return $this->mime;
    }

    public function getEncodedFile()
    {
        return $this->encodedFile;
    }


}
