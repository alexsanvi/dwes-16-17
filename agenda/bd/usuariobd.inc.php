<?php

require_once("bd/bdconnection.inc.php");
require_once("entity/usuario.inc.php");

class UsuarioBD extends BDConnection
{
    public function __construct()
    {
        parent::__construct();
    }

    private function encryptPassword(Usuario $usuario)
    {
        $pass = SHA1( $usuario->getPassword());

        return $pass;
    }

    public function checkLogin(Usuario $usuario)
    {
        $sql = "SELECT * FROM usuario WHERE nick=:nick and password=:password;";

        $a_bind_params = array(
            ':nick'=>$usuario->getNick(),
            ':password'=>$this->encryptPassword($usuario));

        $data = $this->executeQuery($sql, $a_bind_params);

        if (count($data) > 0)
            return true;
        else
            return false;
    }

    public function registraUsuario(Usuario $usuario)
    {
        $nick = $usuario->getNick();
        $password = $this->encryptPassword($usuario);
        $role = $usuario->getRole();
        $avatar = $usuario->getAvatarBase64();
        $mimeAvatar = $usuario->getMimeAvatar();

        $sql = "INSERT INTO usuario (nick, password, role, avatar, mime_avatar) values(:nick, :password, :role, :avatar, :mime_avatar);";
        $a_bind_params = array(
            ':nick' => $nick,
            ':password' => $password,
            ':role' => $role,
            ':avatar' => $avatar,
            ':mime_avatar' => $mimeAvatar);

        return $this->execute($sql, $a_bind_params);
    }

    public function findByNick(Usuario $usuario)
    {
        $sql = "SELECT * FROM usuario WHERE nick=:nick;";

        $a_bind_params = array(
            ':nick'=>$usuario->getNick());

        $data = $this->executeQuery($sql, $a_bind_params);

        if (count($data) > 0)
        {
            $usuario->setId($data[0]['id']);
            $usuario->setRole($data[0]['role']);
            $usuario->setAvatar($data[0]['avatar']);
            $usuario->setMimeAvatar($data[0]['mime_avatar']);

            return true;
        }
        else
            return false;
    }
}