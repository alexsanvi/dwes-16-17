<?php
require_once("bd/bdconnection.inc.php");
require_once("entity/contacto.inc.php");

class ContactoBD extends BDConnection
{
    public function __construct()
    {
        parent::__construct();
    }

    private function getQueryContactos($sql)
    {
        $contactos = array();

        $data = $this->executeQuery($sql);

        foreach ($data as $row)
        {
            $objContacto = new Contacto(
                $row['nombre'],
                $row['telefono'],
                $row['ciudad'],
                $row['contactos_ciudad']);

            $objContacto->setId($row['id']);
            $objContacto->setImagenContacto($row['nombre_imagen']);

            $contactos[] = $objContacto;
        }

        return $contactos;
    }

    public function getContactos()
    {
        $sql = "SELECT * FROM contacto;";

        return $this->getQueryContactos($sql);
    }

    public function getContactoById($id)
    {
        try
        {
            $sql = "SELECT * FROM contacto WHERE id = :id;";

            $data = $this->getById($sql, $id);

            $contacto = new Contacto(
                $data['nombre'],
                $data['telefono'],
                $data['ciudad'],
                $data['contactos_ciudad']);
            $contacto->setId($data['id']);
            $contacto->setImagenContacto($data['nombre_imagen']);
        }
        catch(Exception $ex)
        {
            $contacto = null;
        }

        return $contacto;
    }

    public function buscaContactos($busqueda)
    {
        $sql = "SELECT * FROM contacto WHERE nombre ";
        $sql .= "LIKE '%$busqueda%' or telefono ";
        $sql .= "LIKE '%$busqueda%' or ciudad ";
        $sql .= "LIKE '%$busqueda%' order by nombre asc;";

        return $this->getQueryContactos($sql);
    }

    public function getCiudadesContactos()
    {
        $ciudad = null;

        $sql = "SELECT distinct ciudad FROM contacto;";

        $data = $this->executeQuery($sql);

        $ciudades = array();

        foreach ($data as $row)
        {
            $ciudades[] = $row['ciudad'];
        }

        return $ciudades;
    }

    private function getNumContactosCiudad($ciudad)
    {
        $numContactos = null;

        $sql = "SELECT COUNT(*) FROM contacto WHERE ciudad=:ciudad;";

        $a_bind_param = array(':ciudad' => $ciudad);
        $data = $this->executeQuery($sql, $a_bind_param);

        if (count($data) === 1)
            return (int)$data[0][0];

        return 0;
    }

    private function actualizaNumContactosCiudad($numContactosActual, $ciudad)
    {
        $sql = "UPDATE contacto SET contactos_ciudad=:numContactos WHERE ciudad = :ciudad;";

        $a_bind_params = array(
            ':numContactos' => $numContactosActual,
            ':ciudad' => $ciudad);

        return $this->execute($sql, $a_bind_params);
    }

    public function addContacto(Contacto $contacto)
    {
        $nombre = $contacto->getNombre();
        $telefono = $contacto->getTelefono();
        $ciudad = $contacto->getCiudad();
        $nombreImagen = $contacto->getImagenContacto();

        $numContactosActual = $this->getNumContactosCiudad($ciudad)+1;

        $this->abreTransaccion();

        $sql = "INSERT INTO contacto (nombre, telefono, ciudad, nombre_imagen) values(:nombre, :telefono, :ciudad, :imagen);";
        $a_bind_params = array(
            ':nombre' => $nombre,
            ':telefono' => $telefono,
            ':ciudad' => $ciudad,
            ':imagen' => $nombreImagen);

        $result = $this->execute($sql, $a_bind_params);

        if ($result === true)
        {
            $result = $this->actualizaNumContactosCiudad(
                $numContactosActual, $ciudad);
        }

        $this->cierraTransaccion($result);

        return $result;
    }

    public function removeContacto(Contacto $contacto)
    {
        $id = $contacto->getId();
        $ciudad = $contacto->getCiudad();

        $numContactosActual = $this->getNumContactosCiudad($ciudad)-1;

        $this->abreTransaccion();

        $sql = "DELETE FROM contacto WHERE id=:id;";

        $a_bind_params = array(':id' => $id);
        $result = $this->execute($sql, $a_bind_params);

        if ($result === true)
        {
            $result = $this->actualizaNumContactosCiudad(
                $numContactosActual, $ciudad);
        }

        $this->cierraTransaccion($result);

        return $result;
    }

    public function updateContacto(Contacto $contacto)
    {
        $nombre = $contacto->getNombre();
        $telefono = $contacto->getTelefono();
        $ciudad = $contacto->getCiudad();
        $id = $contacto->getId();

        $contactoAnterior = $this->getContactoById($id);

        $this->abreTransaccion();

        $sql = "UPDATE contacto set nombre=:nombre, telefono=:telefono, ciudad=:ciudad WHERE id=:id;";
        $a_bind_params = array(
            ':nombre' => $nombre,
            ':telefono' => $telefono,
            ':ciudad' => $ciudad,
            ':id' => $id);

        $result = $this->execute($sql, $a_bind_params);

        if ($result === true)
        {
            if ($ciudad !== $contactoAnterior->getCiudad())
            {
                $numContactosAnterior = $this->getNumContactosCiudad($contactoAnterior->getCiudad());

                $result = $this->actualizaNumContactosCiudad(
                    $numContactosAnterior, $contactoAnterior->getCiudad());

                if ($result === true)
                {
                    $numContactosActual = $this->getNumContactosCiudad($ciudad);

                    $result = $this->actualizaNumContactosCiudad(
                        $numContactosActual, $ciudad);
                }
            }
        }

        $this->cierraTransaccion($result);

        return $result;
    }
}