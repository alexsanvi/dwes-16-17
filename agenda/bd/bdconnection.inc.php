<?php

require_once("exceptions/bdexception.inc.php");

class BDConnection
{
    static protected $conexion = null;
    private $lastQueryError;

    public function __construct()
    {
        if (self::$conexion === null)
        {
            try
            {
                require('config.inc.php');

                self::$conexion = new PDO(
                    "mysql:host=$servidor;dbname=$nombreBD",
                    $usuario, $password);
            }
            catch(PDOException $ex)
            {
                echo "<p>Error conectando a la base de datos:";
                exit();
            }
        }
    }

    // $a_bind_params example array(':calories' => $calorías, ':colour' => $color)
    protected function executeQuery($sql, $a_bind_params=array())
    {
        $data = null;

        try
        {
            $consulta = self::$conexion->prepare($sql);
            $result = $consulta->execute($a_bind_params);

            if ($result === false)
                throw new BDException($consulta);

            $data = array();
            while ($arrContacto = $consulta->fetch())
            {
                $data[] = $arrContacto;
            }
        }
        catch(BDException $ex)
        {
            echo "<p>error: ".$ex->getMessage()."</p>";

        }

        return $data;
    }

    public function getById($sql, $id)
    {
        $a_bind_parameters = array(':id' => $id);
        $data = $this->executeQuery($sql, $a_bind_parameters);

        if (count($data) === 1)
            return $data[0];

        throw new Exception("ID no encontrado");
    }

    public function execute($sql, $a_bind_params=array())
    {
        $result = false;
        try
        {
            $consulta = self::$conexion->prepare($sql);
            $result = $consulta->execute($a_bind_params);

            if ($result === false)
            {
                $this->lastQueryError = $consulta->errorInfo();
                throw new BDException($consulta);
            }
        }
        catch(BDException $ex)
        {
        }

        return $result;
    }

    protected function abreTransaccion()
    {
        self::$conexion->beginTransaction();
    }

    protected function cierraTransaccion($estado)
    {
        if ($estado === true)
        {
            self::$conexion->commit();
        }
        else
        {
            self::$conexion->rollback();
        }
    }

    public function getLastError()
    {
        return $this->lastQueryError[2];
    }

    protected function setLastQueryError($consulta)
    {
        $this->lastQueryError = $consulta->errorInfo();
    }
}