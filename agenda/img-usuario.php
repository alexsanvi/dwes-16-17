<?php
    require("partials/checkUserLogued.inc.php");
    require_once("bd/usuariobd.inc.php");
    require_once("entity/usuario.inc.php");

    $usuario = new Usuario($_SESSION['usuario']);
    $usuarioBD = new UsuarioBD();
    if ($usuarioBD->findByNick($usuario) === true)
    {
        header("Content-Type: {$usuario->getMimeAvatar()}");
        echo $usuario->getAvatar();
    }
?>