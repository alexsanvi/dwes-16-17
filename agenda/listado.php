<?php
// Include the main TCPDF library (search for installation path).
include('tcpdf/config/tcpdf_config.php');
include('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Agenda", "listado de contactos", array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

require_once('bd/contactobd.inc.php');
require_once("entity/contacto.inc.php");
require_once('utils/utilsContactos.inc.php');

$contactoBD = new ContactoBD();

$contactos = $contactoBD->getContactos();

$html = <<<EOD
<table>
    <tr>
        <th>IMAGEN</th>
        <th>ID</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Ciudad</th>
        <th>Núm. Contactos Ciudad</th>
    </tr>
EOD;

foreach($contactos as $contacto)
{
    $html .= "<tr>";
    $html .= "<td><img src=\"imgs/subidas/".$contacto->getImagenContacto()."\"></td>";
    $html .= "<td>".$contacto->getId()."</td>";
    $html .= "<td>".$contacto->getNombre()."</td>";
    $html .= "<td>".$contacto->getTelefono()."</td>";
    $html .= "<td>".$contacto->getCiudad()."</td>";
    $html .= "<td>".$contacto->getContactosCiudad()."</td>";
    $html .= "</tr>";
}

$html .= "</table>";
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
