<?php
session_start();

require("partials/cabecera.php");
require("utils/file.inc.php");
?>
<main>
    <form id="registro"
          action="<?php echo $_SERVER['PHP_SELF']; ?>"
          method="post"
          enctype="multipart/form-data">

        <label>Usuario</label>
        <input type="text" name="usuario">

        <label>Password</label>
        <input type="password" name="password">

        <label>Re-Password</label>
        <input type="password" name="password2">

        <label>Avatar</label>
        <input type="file" name="avatar">

        <label>Role</label>
        <select name="role">
            <option value="user">USER</option>
            <option value="admin">ADMIN</option>
        </select>

        <input type="submit" value="Enviar" name="enviar">
    </form>
    <?php
        if (isset($_POST['enviar']))
        {
            if (isset($_POST['usuario']) && trim($_POST['usuario']) !== ''
                && isset($_POST['password']) && trim($_POST['password']) !== ''
                && isset($_POST['password2']) && trim($_POST['password2']) !== ''
                && isset($_FILES['avatar']))
            {
                if ($_POST['password'] === $_POST['password2'])
                {
                    require_once('bd/usuariobd.inc.php');
                    require_once ('entity/usuario.inc.php');

                    $file = new File('avatar');

                    $file->readEncodedImage();

                    $usuario = new Usuario($_POST['usuario'], $_POST['password'], $_POST['role']);

                    $usuario->setAvatar($file->getEncodedFile());
                    $usuario->setMimeAvatar($file->getMime());

                    $usuarioBD = new UsuarioBD();

                    $resultOk = $usuarioBD->registraUsuario($usuario);
                    if ($resultOk === true)
                    {
                        $_SESSION['usuario'] = $_POST['usuario'];

                        header('location: index.php');
                    }
                    else
                    {
                        $mensaje = $usuarioBD->getLastError();
                        echo "<p>$mensaje</p>";
                    }
                }
                else
                {
                    echo "<p>Las contraseñas no coinciden</p>";
                }
            }
            else
            {
                echo "<p>Debes rellenar todos los datos</p>";
            }
        }
    ?>
</main>
<?php
require("partials/pie.php");
?>

