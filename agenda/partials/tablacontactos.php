<table>
    <tr>
        <th>IMAGEN</th>
        <th>ID</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Ciudad</th>
        <th>Núm. Contactos Ciudad</th>
        <th>Operaciones</th>
    </tr>
    <?php
    if (isset($_GET['ciudad']))
        $ciudad = $_GET['ciudad'];
    else
        $ciudad = "";

    foreach($contactos as $contacto)
    {
        if ($ciudad === '' || $contacto->getCiudad() === $ciudad)
        {
            echo "<tr>";
            echo "<td><img src='".getRutaImagenContacto($contacto)."' alt='Imagen contacto'></td>";
            echo "<td>".$contacto->getId()."</td>";
            if (esOperacion('editar') === true
                && isset($_GET['id'])
                && $_GET['id'] == $contacto->getId())
            {
                echo "<td><input type='text' name='nombre' value='".$contacto->getNombre()."' ></td>";
                echo "<td><input type='text' name='telefono' value='".$contacto->getTelefono()."' ></td>";
                echo "<td><input type='text' name='ciudad' value='".$contacto->getCiudad()."' ></td>";
                echo "<td>".$contacto->getContactosCiudad()."</td>";
                echo "<td><input type='submit' name='enviarEdicion' value='Edita'></td>";
            }
            else
            {
                echo "<td>".$contacto->getNombre()."</td>";
                echo "<td>".$contacto->getTelefono()."</td>";
                echo "<td>".$contacto->getCiudad()."</td>";
                echo "<td>".sprintf(ngettext("%d contacto", "%d contactos", $contacto->getContactosCiudad()), $contacto->getContactosCiudad())."</td>";
                echo "<td>";
                echo "<a href='".$_SERVER['PHP_SELF']."?operacion=eliminar&id=".$contacto->getId()."'><img src='imgs/remove.png' width='30' alt='Eliminar'></a>";
                echo "<a href='".$_SERVER['PHP_SELF']."?operacion=editar&id=".$contacto->getId()."'><img src='imgs/edit.jpg' width='30' alt='editar'></a>";
                echo "</td>";
            }
            echo "</tr>";
        }
    }
    ?>

</table>