<?php
    $nombre = "";
    $telefono = "";
    $ciudad = "";
    if (!empty($_POST['enviar']))
    {
        if (!empty($_POST['nombre']))
            $nombre = $_POST['nombre'];

        if (!empty($_POST['telefono']))
            $telefono = $_POST['telefono'];

        if (!empty($_POST['ciudad']))
            $ciudad = $_POST['ciudad'];
    }
?>

<div id="insertar">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>"
          method="post" enctype="multipart/form-data">
        <label>Nombre</label><input type="text" name="nombre" value="<?php echo $nombre;?>">
        <label>Teléfono</label><input type="tel" name="telefono" value="<?php echo $telefono;?>">
        <label>Ciudad</label><input type="text" name="ciudad" value="<?php echo $ciudad;?>">
        <label>Imagen</label><input type="file" name="imgContacto">
        <input type="submit" value="enviar" name="enviar">
    </form>
</div>

<div id="busqueda">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label>Buscar</label><input type="text" name="buscar"> <!--value="<?php // echo $buscar;?>"-->
        <input type="submit" value="buscar" name="enviarBuscar">
    </form>
</div>