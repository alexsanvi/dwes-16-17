<?php
    if (isset($_GET["language"]))
        $language = trim(strip_tags($_GET["language"]));
    else
    {
        if (isset($_SESSION["language"]))
            $language = $_SESSION["language"];
        else
            $language = "es_ES";
    }

    $_SESSION["language"] = $language;
    $language .= ".utf8";

    putenv("LC_ALL=$language");
    setlocale(LC_ALL, $language);

    bindtextdomain("messages", "./locale");
    bind_textdomain_codeset("messages", "UTF-8");

    textdomain("messages");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset=""utf-8">
        <title><?php echo _("Agenda de contactos")?></title>
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/estilos.css">
    </head>
<body>
    <header>
        <h1><?php echo _("Agenda de contactos")?></h1>
        <time>
        <?php
            echo "<p>" . strftime("Fecha: %A, %#d de %B de %Y") . "</p>";
            echo "<p>" . $_SERVER['HTTP_ACCEPT_LANGUAGE'] . "</p>";
        ?>
        </time>
        <?php
            if (isset($_SESSION['usuario']))
            {
                echo '<img class="img-circle" src="img-usuario.php" alt="'._("Imagen usuario").'">';
            }
        ?>
    </header>
    <nav>
        <ul>
            <li><a href="index.php"><?php echo _("Agenda")?></a></li>
            <li><a href="registro.php"><?php echo _("Registrate") ?></a></li>
            <?php
                if (isset($_SESSION['usuario']))
                {
                    echo "<li><a href='listado.php'>"._("Listado")."</a></li>";
                    echo "<li><a href='logout.php'>"._("Salir")."</a></li>";
                }
                else
                {
                    echo "<li><a href='login.php'>"._("Conecta")."</a></li>";
                }
            ?>
            <li><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?language=en_GB">
                English</a></li>
            <li><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?language=es_ES">
                Español</a></li>
        </ul>
    </nav>
