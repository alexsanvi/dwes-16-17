<?php
function miGestorDeErrores($errno, $errstr)
{
    /* Observa que los parámetros están cambiados en la excepción y la
    función */
    throw new Exception($errstr, $errno);
}

set_error_handler("miGestorDeErrores");

try
{
    $dividendo = 10;
    $divisor = 0;
    $resultado = $dividendo / $divisor;
    echo $resultado;
}
catch(Exception $e)
{
    echo $e->getMessage()."<br />";
}