<?php
// Iniciamos la sesión o recuperamos la anterior sesión existente
session_start();

if (isset($_GET['op']) && $_GET['op'] === 'vaciar')
{
    $_SESSION['visitas'] = null;
    unset($_SESSION['visitas']);
}

// Comprobamos si la variable ya existe
if (isset($_SESSION['visitas']))
{
    $_SESSION['visitas']++;
    $_SESSION['visitaAnterior'] = mktime();
}
else
    $_SESSION['visitas'] = 1;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset=""utf-8">
    <title>Visitas</title>
</head>
<body>
    <p>Num visitas: <?php echo $_SESSION['visitas']; ?></p>
    <p>Visita anterior: <?php echo $_SESSION['visitaAnterior']; ?></p>
    <a href="<?php echo $_SERVER['PHP_SELF'].'?op=vaciar' ?>">Vaciar sesión</a>
</body>
</html>
