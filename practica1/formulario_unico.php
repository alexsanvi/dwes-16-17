<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Desarrollo web en entorno servidor - tema 2 - actividad
        5</title>
    <meta name="description" content="PHP, PHPStorm">
    <meta name="author" content="Alejandro Amat">
</head>
<body>
<?php
    $error = array();

    if (isset($_POST['enviar']))
    {
        if (!empty($_POST['nombre']))
        {
            $nombre = $_POST['nombre'];
            echo "Nombre: ".$nombre."<br />";
        }
        else
        {
            $error[] = "Debe rellenar el nombre";
        }

        if (!empty($_POST['modulos']))
        {
            $modulos = $_POST['modulos'];

            foreach ($modulos as $modulo)
            {
                echo "Modulo: ".$modulo."<br />";
            }
        }
        else
        {
            $error[] = "Sin módulos";
        }
    }

    if (!empty($error))
    {
        foreach($error as $e)
            echo "<p>$e</p>";
    }

    ?>
    <form name="input" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        Nombre del alumno: <input type="text" name="nombre"/><br />
        <p>Módulos que cursa:</p>
        <input type="checkbox" name="modulos[]" value="DWES" />
        Desarrollo web en entorno servidor<br />
        <input type="checkbox" name="modulos[]" value="DWEC" />
        Desarrollo web en entorno cliente<br />
        <br />
        <input type="submit" value="Enviar" name="enviar"/>
    </form>
</body>
</html>