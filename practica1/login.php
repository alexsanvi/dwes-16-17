<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Desarrollo web en entorno servidor - tema 2 - actividad
        5</title>
    <meta name="description" content="PHP, PHPStorm">
    <meta name="author" content="Alejandro Amat">
</head>
<body>
<?php
    $user = '';
    $password = '';
    if (isset($_POST['enviar']))
    {
        if (empty($_POST['user']) && empty($_POST['password']))
        {
            echo "Debes introducir los datos";
        }
        else
        {
            if (empty($_POST['user']))
            {
                echo "Debes introducir el usuario";
                $password = $_POST['password'];
            }
            elseif (empty($_POST['password']))
            {
                echo "Debes introducir el password";
                $user = $_POST['user'];
            }
            else
            {
                $password = $_POST['password'];
                $user = $_POST['user'];

                echo "Bienvenido $user";
                if (isset($_POST['recuerdame']))
                    echo "<br>recuérdame: ".$_POST['recuerdame'];
                else
                    echo "<br> no me recuerdes";
            }
        }
    }
?>
<form id="login" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
    <label>User</label><input type="text" name="user" value="<?php echo $user; ?>"/><br />
    <label>Password</label><input type="password" name="password" value="<?php echo $password; ?>"/><br />
    <input type="checkbox" name="recuerdame" value="1">recuérdame
    <input type="submit" value="Enviar" name="enviar"/>
</form>
</body>
</html>