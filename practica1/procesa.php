<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Desarrollo web en entorno servidor - tema 2 - actividad
        5</title>
    <meta name="description" content="PHP, PHPStorm">
    <meta name="author" content="Alejandro Amat">
</head>
<body>
<?php

    $nombre = $_POST['nombre'];
    echo "Nombre: ".$nombre."<br />";

    if (!empty($_POST['modulos']))
    {
        $modulos = $_POST['modulos'];

        foreach ($modulos as $modulo)
        {
            echo "Modulo: ".$modulo."<br />";
        }
    }
?>
</body>
</html>